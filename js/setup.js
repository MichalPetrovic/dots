var Setup = function () {
    this._dom = {
        container: document.querySelector("#setup"),
        local: {
            container: document.querySelector("#local"),
            players: document.querySelector("#local-players"),
            roster: document.querySelector("#local-roster"),
            play: document.querySelector("#local-play"),
            template: null,
            rosterItems: []
        }
    }

    this._setupLocal();
    this._load();
}

Setup.prototype._setupLocal = function () {
    this._dom.local.template = this._dom.local.roster.querySelectorAll("p")[0];
    this._dom.local.template.parentNode.removeChild(this._dom.local.template);

    this._dom.local.play.addEventListener("click", this._playLocal.bind(this));
    this._dom.local.players.addEventListener("change", this._syncRoster.bind(this));
}

Setup.prototype._load = function () {
    var players = localStorage.getItem("local-players") || 2;
    this._dom.local.players.value = players;
    this._syncRoster();

    for (var i = 0; i < players; i++) {
        var item = this._dom.local.rosterItems[i];
        var type = localStorage.getItem("local-" + i + "-type") || "ui";
        var name = localStorage.getItem("local-" + i + "-name") || ("Player" + Math.round(100 * Math.random()));

        item.querySelectorAll("input")[0].value = name;
        item.querySelectorAll("select")[0].value = type;
    }


}

Setup.prototype._save = function () {
    /* local */
    localStorage.setItem("local-players", this._dom.local.players.value);
    for (var i = 0; i < this._dom.local.rosterItems.length; i++) {
        var item = this._dom.local.rosterItems[i];
        var name = item.querySelectorAll("input")[0].value;
        var type = item.querySelectorAll("select")[0].value;
        localStorage.setItem("local-" + i + "-type", type);
        localStorage.setItem("local-" + i + "-name", name);
    }
}

Setup.prototype._syncRoster = function () {
    var players = parseInt(this._dom.local.players.value);

    while (this._dom.local.rosterItems.length < players) {
        var item = this._dom.local.template.cloneNode(true);
        var span = item.querySelectorAll("span")[0];
        this._dom.local.rosterItems.push(item);
        span.innerHTML = this._dom.local.rosterItems.length + ".";
        this._dom.local.roster.appendChild(item);
    }

    while (this._dom.local.rosterItems.length > players) {
        var item = this._dom.local.rosterItems.pop();
        item.parentNode.removeChild(item);
    }

}



Setup.prototype._playLocal = function () {
    this._end();
    var game = new Atoms.Local();

    for (var i = 0; i < this._dom.local.rosterItems.length; i++) {
        var item = this._dom.local.rosterItems[i];
        var name = item.querySelectorAll("input")[0].value || "[noname]";
        var type = item.querySelectorAll("select")[0].value;
        if (type == "ui") {
            game.addPlayerUI(name);
        } else {
            game.addPlayerAI(name);
        }
    }

    game.start();
}


Setup.prototype._end = function () {
    this._save();
    this._dom.container.parentNode.removeChild(this._dom.container);
    document.body.style.backgroundImage = "none";
}
