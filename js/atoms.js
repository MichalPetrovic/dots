var Atoms = function () {
    this._board = new Board(6, 6);
    this._audio = new Audio();

    this._players = [];
    this._colors = ["blue", "red", "green", "orange"];
    this._canvas = new Canvas(this._board, 55, 35, this._colors);/* Set the size of canvas!!*/
    
    

    var table = document.createElement("table");
    table.id = "score";
    table.innerHTML = "<tbody><tr></tr><tr></tr></tbody>";
    this._scoreContainer = table;
    this._scores = [];
    this._names = [];
    this._reactionDelay = 200;

    document.body.appendChild(this._scoreContainer);
    document.body.appendChild(this._canvas.getCanvas());

    this._canvas.prepare();

    this._currentPlayer = -1;
}

Atoms.prototype.start = function () {}

Atoms.prototype._addPlayer = function (player) {
    var tr1 = this._scoreContainer.querySelectorAll("tr")[0];
    var tr2 = this._scoreContainer.querySelectorAll("tr")[1];
    if (this._players.length) {
        var td = document.createElement("td");
        td.innerHTML = ":";
        tr1.appendChild(td);
        var td = document.createElement("td");
        td.innerHTML = ":";
        tr2.appendChild(td);
    }

    var color = this._colors[this._players.length];
    var name = document.createElement("td");
    name.style.color = color;
    name.innerHTML = player.getName();
    var score = document.createElement("td");
    score.style.color = color;
    score.innerHTML = 0;

    tr1.appendChild(name);
    tr2.appendChild(score);
    this._names.push(name);
    this._scores.push(score);

    this._players.push(player);
    return this;
}

Atoms.prototype._nextPlayer = function () {
    this._updateScore();
    if (this._currentPlayer > -1) {
        this._names[this._currentPlayer].style.textDecoration = "";
    }

    do { /* find next playing player */
        this._currentPlayer = (this._currentPlayer + 1) % this._players.length;
    } while (!this._players[this._currentPlayer]);

    if (this._currentPlayer > -1) {
        this._names[this._currentPlayer].style.textDecoration = "underline";
    }
    this._players[this._currentPlayer].play(this._board).then(this._playerCallback.bind(this));
}

Atoms.prototype._updateScore = function () {
    var zeros = [];
    var total = 0;
    var max = this._board.getWidth() * this._board.getHeight();

    for (var i = 0; i < this._players.length; i++) {
        var score = this._board.getScore(i) || 0;
        total += score;
        this._scores[i].innerHTML = score;
        if (score == 0) {
            zeros.push(i);
        }
    }

    if (total == max) { /* disable players with score=0 */
        while (zeros.length) {
            this._players[zeros.pop()] = null;
        }
    }
}

Atoms.prototype._playerCallback = function (xy) {
    var number = this._board.getPlayer(xy);
    if (number > -1 && number != this._currentPlayer) {
        throw new Error("Player " + this._currentPlayer + " made an illegal move to " + xy);
    }

    this._board.setAtoms(xy, this._board.getAtoms(xy) + 1, this._currentPlayer);
    this._canvas.draw(xy);
    this._check(0);
}

Atoms.prototype._check = function (depth) {
    this._updateScore();
    var winner = this._board.getWinner();

    if (winner > -1) {
        this._announceWinner(winner);
        return;
    }

    if (this._board.hasCriticals()) {
        setTimeout(this._react.bind(this, depth), this._reactionDelay);
        return;
    }

    this._nextPlayer();
}

Atoms.prototype._announceWinner = function (winner) {
    this._audio.stop();
    if (confirm("Winner: " + this._players[winner].getName() + " Play again?")) {
        window.location.reload();
    } else {
        window.location.assign("../../index.html");
    }
}

Atoms.prototype._react = function (depth) {
    if (this._reactionDelay) {
        this._audio.play(depth);
        setTimeout(function () {
            this._audio.stop();
        }.bind(this), this._reactionDelay * 0.5);
    }

    var changed = this._board.react();
    for (var i = 0; i < changed.length; i++) {
        this._canvas.draw(changed[i]);
    }
    this._check(depth + 1);
}

/**/

Atoms.Local = function () {
    Atoms.call(this);
}
Atoms.Local.prototype = Object.create(Atoms.prototype);

Atoms.Local.prototype.addPlayerUI = function (name) {
    var player = new Player.UI(this._players.length, name);
    return this._addPlayer(player);
}

Atoms.Local.prototype.addPlayerAI = function (name) {
    var player = new Player.AI(this._players.length, name);
    return this._addPlayer(player);
}

Atoms.Local.prototype.start = function () {
    var allAI = true;
    for (var i = 0; i < this._players.length; i++) {
        if (!(this._players[i] instanceof Player.AI)) {
            allAI = false;
        }
    }
    if (allAI) {
        this._reactionDelay = 0;
    }
    this._nextPlayer();
}

/**/
